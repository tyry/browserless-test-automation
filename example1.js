const puppeteer = require("puppeteer");

const scrape = async () => {
    const browser = await puppeteer.launch({headless: false});
    // const browser = await puppeteer.connect({ browserWSEndpoint: 'wss://chrome.browserless.io?token=[ADD BROWSERLESS API TOKEN HERE]' })
 
    const page = await browser.newPage();

    await page.goto('https://trypap.com/');
   
    await page.setViewport({ width: 1920, height: 1080 });
   
    await page.type('input[placeholder="Please Enter a Password"]', 'BrowserlessIsCool1234');
   
    await page.waitForTimeout(2000);
   
    await page.screenshot({ path: "screenshot.png", fullPage: true });
    
    await browser.close();
};
scrape();